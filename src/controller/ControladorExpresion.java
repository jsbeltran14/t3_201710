package controller;

import model.data_structures.PilaLista;
import model.logic.LogicaNegocio;

public class ControladorExpresion {

	private LogicaNegocio log;
	
	public ControladorExpresion(){
		log = new LogicaNegocio();
	}
	
	public boolean isGood(String expre){
		return log.expresionBienFormada(expre);
	}
	
	public void OrdenarPila(PilaLista<Character> pilaExpresion){
		log.OrdenarPila(pilaExpresion);
	}
	
}
