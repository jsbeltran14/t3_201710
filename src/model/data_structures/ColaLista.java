package model.data_structures;

import java.util.Iterator;

public class ColaLista<T> implements ICola<T>{
	
	private NodoSimple<T> primero;

	private NodoSimple<T> ultimo;

	private int size;
	
	private class IteradorCola<T> implements Iterator<T>{

		private NodoSimple<T> actual;
		
		public IteradorCola(NodoSimple<T> primero){
			actual = primero;
		}
		
		public boolean hasNext() {
			// TODO Auto-generated method stub
			if(actual.darSiguiente()!=null){
				return true;
			}
			return false;
		}

		public T next() {
			// TODO Auto-generated method stub
			if(hasNext()){
				T elem = actual.darElemento();
				actual = actual.darSiguiente();
				return elem;
			}
			return null;
		}
		
		public NodoSimple<T> darActual(){
			return actual;
		}

	}

	public Iterator<T> iterator() {
		IteradorCola<T> iter = new IteradorCola<T>(primero);
		return iter;
	}

	public ColaLista(){
		primero = null;
		ultimo = null;
		size = 0;
	}
	
	public void enqueue(T elem){
		// mismo problema de posicion de la pila
		NodoSimple<T> newNodo = new NodoSimple<T>(elem,0);
		if(size == 0){
			primero = newNodo;
			ultimo = newNodo;
			size++;
		}
		else{
			ultimo.cambiarSiguienet(newNodo);
			ultimo=newNodo;
			size++;
		}
	}
	
	public T dequeue(){
		if(primero.darSiguiente()!=null){
			T aRet = primero.darElemento();
			primero = primero.darSiguiente();
			size--;
			return aRet;
			
		}
		else{
			T aRet = primero.darElemento();
			primero = null;
			ultimo = null;
			size--;
			return aRet;
		}
	}
	
	public boolean isEmpty(){
		return size == 0;
	}
	
	public int size(){
		return size;
	}
	
	public NodoSimple<T> darPrimero(){
		return primero;
	}
	
	public NodoSimple<T> darUltimo(){
		return ultimo;
	}
	

}

