package model.data_structures;

public interface ICola<T> extends Iterable<T>{
	
	public void enqueue (T pElem);

	public T dequeue ();

	public boolean isEmpty();

	public int size();

}
