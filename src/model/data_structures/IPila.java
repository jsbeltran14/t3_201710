package model.data_structures;

public interface IPila<T> extends Iterable<T>{

	public void push(T pElem); 

	public T pop();

	public boolean isEmpty();

	public int size();

}
