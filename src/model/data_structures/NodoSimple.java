package model.data_structures;

public class NodoSimple<T> {
	
	private NodoSimple<T> siguiente;
	
	private T elemento;
	
	private int pos;

	public NodoSimple(T pElem, int pPos){
		elemento = pElem;
		siguiente = null;
		pos = pPos;
	}
	
	public T darElemento(){
		return elemento;
	}
	
	public int darPos(){
		return pos;
	}
	
	public NodoSimple<T> darSiguiente(){
		return siguiente;
	}
	
	public void agregarElem(T pElem){
		elemento = pElem;
	}
	
	public void cambiarSiguienet(NodoSimple<T> pNodo){
		siguiente = pNodo;
	}
}
