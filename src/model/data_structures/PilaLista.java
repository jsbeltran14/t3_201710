package model.data_structures;

import java.util.Iterator;

public class PilaLista<T> implements IPila<T>{

	private class IteradorLista<T> implements Iterator<T>{

		private NodoSimple<T> actual;
		
		public IteradorLista(NodoSimple<T> primero){
			actual = primero;
		}
		
		public boolean hasNext() {
			// TODO Auto-generated method stub
			if(actual.darSiguiente()!=null){
				return true;
			}
			return false;
		}

		public T next() {
			// TODO Auto-generated method stub
			if(hasNext()){
				T elem = actual.darElemento();
				actual = actual.darSiguiente();
				return elem;
			}
			return null;
		}
		
		public NodoSimple<T> darActual(){
			return actual;
		}

	}
	
	private NodoSimple<T> primero;
	
	private NodoSimple<T> ultimo;
	
	private int size;
	
	 public PilaLista(){
		primero = null;
		ultimo = null;
		size = 0;
				
	}
	
	public void push(T elem){
		if(size == 0){
			NodoSimple<T> newNodo = new NodoSimple<T>(elem, 0);
			primero = newNodo;
			ultimo = newNodo;
			size++;
		}
		else{
			//problema en pos
			NodoSimple<T> newNodo = new NodoSimple<T>(elem, 0);
			newNodo.cambiarSiguienet(primero);
			primero = newNodo;
			size++;
			
		}
	}
	
	public T pop(){
		if(primero.darSiguiente()!=null){
			T aRet = primero.darElemento();
			primero = primero.darSiguiente();
			size--;
			return aRet;
			
		}
		else{
			T aRet = primero.darElemento();
			primero = null;
			ultimo = null;
			size--;
			return aRet;
		}
	}
	
	public boolean isEmpty(){
		return size == 0;
		
	}
	
	public int size(){
		return size;
	}
	
	public NodoSimple<T> darPrimero(){
		return primero;
	}
	
	public NodoSimple<T> darUltimo(){
		return ultimo;
	}

	public Iterator<T> iterator() {
		IteradorLista<T> iter = new IteradorLista<T>(primero); 
		return iter;
	}
}
