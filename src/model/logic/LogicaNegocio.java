package model.logic;

import model.data_structures.ColaLista;
import model.data_structures.PilaLista;

public class LogicaNegocio {
	
	private PilaLista<Character> pilaExpresion;
	
	public LogicaNegocio(){
		pilaExpresion = null;
	}
	
	public boolean expresionBienFormada (String expresion){
		char[] expre = expresion.toCharArray();
		
		if(expresion.indexOf("]")>expresion.indexOf("[")){
			for(int i = 0; i < expre.length; i++){
				pilaExpresion.push(expre[i]);
			}
			return true;
		}
		if(expresion.indexOf(")")>expresion.indexOf("(")){
			
			for(int i = 0; i < expre.length; i++){
				pilaExpresion.push(expre[i]);
			}
			return true;
		}
		if(expresion.indexOf(")")<0 && expresion.indexOf("(")<0){
			for(int i = 0; i < expre.length; i++){
				pilaExpresion.push(expre[i]);
			}
			return true;
		}
	
		return false;
	}
	
	public void OrdenarPila(PilaLista<Character> pilaExpresion){
		ColaLista<Character> aux = new ColaLista<Character>();
		PilaLista<Character> aux2 = new PilaLista<Character>();
		
		for(int i = 0; i<pilaExpresion.size(); i++){
			char act = pilaExpresion.pop();
			if(Character.isDigit(act)){
				aux2.push(act);
			}
			else{
				aux.enqueue(act);
			}
		}
		for(int i = 0; i<aux.size(); i++){
			char act = aux.dequeue();
			if(Character.isLetter(act)){
				aux2.push(act);
			}
			else{
				aux.enqueue(act);
			}
		}
		
		this.pilaExpresion=aux2;
	}
	
	public PilaLista<Character> darPila(){
		return pilaExpresion;
	}

	
	
}
