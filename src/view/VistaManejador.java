package view;

import java.util.Scanner;

import model.data_structures.PilaLista;
import controller.ControladorExpresion;

public class VistaManejador {

	private static ControladorExpresion control;
	private static PilaLista<Character> pil;
	
	public VistaManejador(){
		control = new ControladorExpresion();
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc=new Scanner(System.in);
		String expresion = sc.next();
		
		if(control.isGood(expresion)){
			control.OrdenarPila(pil);
			for(int i = 0; i<pil.size(); i++){
				System.out.print(pil.pop());
			}
		}
		

	}

}
